﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BrickConfig {

	[SerializeField] int startStrength;
	[SerializeField] Sprite image;
	[SerializeField] int points;
	[SerializeField] int level;
	[SerializeField] GameObject explosionParticlePrefab;

	public int StartStrength { get { return startStrength; } }
	public Sprite Image { get { return image; } }
	public int Points { get { return points; } }
	public int Level { get { return level; } }
	public GameObject ExplosionParticlePrefab { get { return explosionParticlePrefab; } }



}
