﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatabaseManager : MonoBehaviour
{

	private static DatabaseManager instance;
	[SerializeField] Database database;

	public Database Database { get { return database; } }

	void Awake ()
	{
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}  
	}

	public static DatabaseManager Instance {
		get {
			if (instance == null) {
				instance = FindObjectOfType (typeof(DatabaseManager)) as DatabaseManager;
			}
			return instance;
		}
	}

}
