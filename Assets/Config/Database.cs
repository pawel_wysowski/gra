﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Wysu/Database", order = 1)]
public class Database : ScriptableObject {

	[SerializeField] List<BrickConfig> brickConfigs;

	public List<BrickConfig> BrickConfigs { get { return brickConfigs; } }

}
