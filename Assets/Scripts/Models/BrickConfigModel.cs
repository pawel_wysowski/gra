﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class BrickConfigModel : Model<BreakoutApplication> {

	public BrickConfig GetBrickConfig(int index){
		return DatabaseManager.Instance.Database.BrickConfigs [index];
	}

	public BrickConfig GetBrickLevel(int level){
		return DatabaseManager.Instance.Database.BrickConfigs [level];
	}

}
