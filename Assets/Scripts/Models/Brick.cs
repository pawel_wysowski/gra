﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Brick {

	public int ConfigIndex;
	public int LevelIndex;
	public Vector3 Position;
	public int Strength = 2;

	public void MoveDown(){
		Position.y--;
	}

	public void AddStrength(){
		Strength++;
	}

	public void SubstractStrength(){
		Strength--;
	}
}
