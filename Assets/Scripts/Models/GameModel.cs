﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;


public class GameModel : Model<BreakoutApplication> {

	public const string BRICK_STRENTGH_CHANGED = "GameModel.StrengthChanged";
	public const string PLAYER_POINTS_CHANGED = "GameModel.PlayerPointsChanged";
	public const string TIME_LEFT_ZERO = "GameModel.TimeLeftZero";
	public const string BRICKS_CHANGED = "GameModel.BricksChanged";
	public const string PLAYER_LIVES_CHANGED = "GameModel.PlayerLivesChanged";
	public const string STRENGTH_ADDED_TO_BRICK = "GameModel.StrengthAddedToBrick";
	public const string BRICK_ON_BOTTOM = "GameModel.BrickOnBottom";
	public const string BRICK_DESTROYED = "GameModel.BrickDestroyed";
	public const string NEXT_LEVEL = "GameModel.NextLevel";
	public const string END_GAME = "GameModel.EndGame";
	public const string BRICKS_DESTROYED = "GameModel.BricksDestroyed";

	public BrickConfigModel BrickConfigModel { get { return brickConfigModel = Assert<BrickConfigModel>(brickConfigModel); } }
	private BrickConfigModel brickConfigModel;


	public int PlayerPoints = 0;
	public int PlayerLives = 3;
	public List<Brick> Bricks;
	public float BulletMovementSpeed = 1.0f;
	public float PlayerMovementSpeed = 10;
	private float timeLeft = 10.0f;
	public float TimeToBuildUp = 5.0f;
	public int level = 1;
	public int index = 0;
	public int HighScore = 0;
	public GameState CurrentState;

	public enum GameState {
		InMenu,
		InGame,
	}

	public void PlayerLivesDown(List<Brick> bottomBricks){
		PlayerLives= PlayerLives - bottomBricks.Count;
		Notify (PLAYER_LIVES_CHANGED);
		if (PlayerLives <= 0)
			Notify (END_GAME);
	}

	public void SubstractStrength(Brick brick){
	
		brick.SubstractStrength ();

		if (brick.Strength > 0) {
			Notify (BRICK_STRENTGH_CHANGED, brick);
		} else {
			Notify (BRICK_DESTROYED, brick);
		}

	}

	public void MoveDownBricks(){
		foreach(Brick brick in  app.model.GameModel.Bricks){
			brick.MoveDown();
		}
			
		List<Brick> bottomBricks = GetBottomBricks ();
		if (bottomBricks.Count > 0) {
			Notify (BRICK_ON_BOTTOM, bottomBricks);
		}

		Notify (BRICKS_CHANGED);
	}
		

	public void Update(){
		if (CurrentState == GameState.InGame) {
			timeLeft -= Time.deltaTime;
			if (timeLeft < 0) {
				timeLeft = 10.0f;
				Notify (TIME_LEFT_ZERO);
			}
			TimeToBuildUp -= Time.deltaTime;
			if (TimeToBuildUp < 2) {
				TimeToBuildUp = 5.0f;
				AddStrengthToRandomBrick ();
			}
			CheckIfNull ();
			if (Input.GetKeyDown (KeyCode.Escape))
				Notify (END_GAME);
		}
	}

	public void AddPlayerPoint(int pointsToAdd){
		PlayerPoints+=pointsToAdd;
		Notify (PLAYER_POINTS_CHANGED);
	}

	public void CheckIfNull(){
		if(app.view.GameView.BrickViews.Count == 0)
			Notify (BRICKS_DESTROYED);
	}

	public void ChangeState(){
		switch (CurrentState){
		case GameState.InGame:
			CurrentState = GameState.InMenu;
			break;
		case GameState.InMenu:
			CurrentState = GameState.InGame;
			break;
		}
	}

	public void ResetLevel(){
		level = 1;
	}

	public void ResetPlayerPoints(){
		PlayerPoints = 0;
		Notify (PLAYER_POINTS_CHANGED);
	}	

	public void ResetPlayerLives(){
		PlayerLives = 3;
		Notify (PLAYER_LIVES_CHANGED);
	}

	public int GetLevelIndex(){
		return level;
	}



	List<Brick> GetBottomBricks(){
		List<Brick> bottomBricks = new List<Brick> ();
		foreach (Brick brick in Bricks) {
			if (brick.Position.y == 0) {
				bottomBricks.Add (brick);
			}
		}
				return bottomBricks;
	}

	public void AddStrengthToRandomBrick(){
		Brick brickModel = getRandomBrickFromList();
		brickModel.AddStrength();
		Notify(STRENGTH_ADDED_TO_BRICK, brickModel);
	}

	public void IncreaseLevel(){
		index++;
		if (index > 2)
			index = 0;
		level++;

	}

	public void CheckLevelValue(){
		if (level == 4)
			Notify (END_GAME);
		else
			Notify (NEXT_LEVEL);
	}

	public Brick getRandomBrickFromList(){
		int r = Random.Range(0,Bricks.Count);
		return Bricks[r];
	}
		

	public void AddBricksToList(){
				Bricks.Clear ();
			for (int j = -5; j < 5; j++) {
				Brick brick = new Brick ();
			brick.ConfigIndex = index;
			brick.LevelIndex = level;
			int y = Random.Range (2, 5);
			brick.Position = new Vector3 (brick.Position.x+j, y , 0);
				Bricks.Add (brick);
			}
		}

	public void DestroyBottomBricks(List<Brick> bottomBricks){
		foreach(Brick brick in bottomBricks){
			Bricks.Remove (brick);
		}
	}

	public void SetHighScore(){
		if (PlayerPoints > HighScore)
			HighScore = PlayerPoints;
		
	}
		

	public void DestroyBrickModel(Brick brick){
		if (Bricks.Contains (brick)) {
			Bricks.Remove (brick);
		}
	}

	public void ChangeLanguage(){
		if (LocaleManager.Instance.ChoosenLanguage == SystemLanguage.English) {
			LocaleManager.Instance.SelectLocale (SystemLanguage.Polish);
		}
		else
		 LocaleManager.Instance.SelectLocale (SystemLanguage.English);
		}
	}

