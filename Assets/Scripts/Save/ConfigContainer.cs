﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/**
 * ConfigContainer is the part of data that isn't change
 * during game. It contains costs of build or upgrade buildings,
 * time of upgrade, skill trainings cost and time, list of items,
 * buildings, decorations, categories etc.
 * 
 * ConfigContainer delivers lists of data (I[element]Data), which are obtained
 * from two sources:
 * - each I[element]Data interface should have corresponding classes [element]
 * and [element]Object.
 * 
 * [element]Object implements I[element]Data and extends ScriptableObject,
 * so it can be created end edited in Unity Editor, and delivered with application.
 * 
 * [element] class implements I[element]Data, and are [Serializable], so can
 * be transfered to game to be added to or override existed config object.
 * 
 * !! CURRENTLY ONLY [element]Object FUNCTIONALITY WORKS - NO DOWNLOADING CONFIGS FROM SERVER OR OTHER PLACE !!
 * (but it's a nice future-proof concept).
 * 
 * When app starts it copies [element]Object and(in future) [element]
 * objects into I[element]Data list, and provides this list to rest of application.
 * 
 * (This would be much simplier if ScriptableObject were marked as [Serializable]
 */
[Serializable]
public class ConfigContainer {

	#region MEMBERS
	public const string DOWNLOADED_CONFIG_PREFS_NAME = "CofigContainer.Override";

	#pragma warning disable 0649 //assigned in editor or by json deserialization
//	List<IAnimalData> _animal;
	//[SerializeField] List<BrickConfig> _brickConfig;


#pragma warning restore 0649
    #endregion

    #region PROPERTIES
//    public List<IAnimalData> AnimalContainer { get { return this._animal; } }

    #endregion

    #region METHODS
    public void InitDatas() {
		/** At start of app.
		 * 1. create empty I[element]Data lists
		 * (see next commnet)
		 */
//		this._animal = new List<IAnimalData>();


       

		/**
		 * (see previous comment)
		 * 2. Copies all [element]Object datas to those lists.
		 * (method _CopyObjects was needed, otherwise I couldnt
		 * get this to work properly.
		 */
//		this._CopyObjects<IAnimalData, AnimalObject>(_animal, _animalObject);

    }

	void _CopyObjects<V, T>(List<V> destination, List<T> source) where T:V {
		if(source == null)
			return;

		if(destination == null) {
			destination = new List<V>();
		}

		for(int i = 0; i < source.Count; i++) {
			destination.Add(source[i]);
		}
	}



	#endregion
}