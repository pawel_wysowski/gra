﻿using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Text))]
public class LocalizedTextComponent : MonoBehaviour {
	#region MEMBERS

	[SerializeField] private string translationKey;

	private Text _cachedText;
	#endregion

	#region PROPERTIES
	private Text CachedTextComponent {
		get {
			if (_cachedText == null) {
				_cachedText = gameObject.GetComponent<Text> ();
			}
			return _cachedText;
		}
	}
	#endregion

	#region METHODS

	private void Start() {
		LocaleManager.Instance.OnLocaleChanged += this.LocalizeComponent;
		this.LocalizeComponent ();
	}

	private void OnDestroy() {
		LocaleManager.Instance.OnLocaleChanged -= this.LocalizeComponent;
	}

	private void LocalizeComponent() {
		this.CachedTextComponent.text = LocaleManager.Instance.GetLocalizedString (translationKey);
	}
	#endregion
}
