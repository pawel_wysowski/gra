﻿#define LOCALE_VERBOSE
using UnityEngine;
using System.Collections.Generic;
using thelab.mvc;

public class LocaleManager : MonoBehaviour {

	#region EVENTS
	/**
	 * Invoked when locale first selected (on startup)
	 * and every time locale changes.
	 */
	public event System.Action OnLocaleChanged;
	#endregion

	#region MESSAGES
	const string ERR_NO_FILE_ATTACHED = "No locales file attached to LocaleController";
	const string WARN_ZERO_LENGHT_KEYS = "Empty array as parameter in LocaleManager.GetLocalizedString";
	const string WARN_KEY_NOT_FOUND = "Trying to get locales for '{0}', but key wasn't found";
	const string DBG_LOCALES_SELECTED = "Selected Locales: {0}";
	const string DBG_FOUND_KEYS = "Found {0} keys";
	const string DBG_FOUND_LANGS = "Found Langs: {0}";
	#endregion

	#region MEMBERS
	const string PREFS_NAME = "LocaleManager.ChoosenLanguage2";
    const string LINE_SEPARATOR_WINDOWS = "\r\n";
    const string LINE_SEPARATOR_UNIX = "\n";
	const char CSV_SEPARATOR = ';';
	static LocaleManager instance = null;

	[Header("Config")]
	[SerializeField] private TextAsset csvFile;
	[SerializeField] private SystemLanguage defaultFallbackLanguage = SystemLanguage.English ; // English maybe?

	SystemLanguage _ChoosenLanguage;
	#endregion

	#region PROPERTIES

	public static LocaleManager Instance { get { return instance; } }

	public SystemLanguage ChoosenLanguage {
		get { return this._ChoosenLanguage; }
		set {
			bool changed = this._ChoosenLanguage != value;
			this._ChoosenLanguage = value;
			if (changed && this.OnLocaleChanged != null) {
				this.OnLocaleChanged.Invoke ();
			}
		}
	}

	Dictionary<string, string> SelectedLocales {
		get { return this.AllLoadedLocales [SelectedLocalesKey]; }
	}

	Dictionary<string, Dictionary<string, string>> AllLoadedLocales {
		get;
		set;
	}

	string SelectedLocalesKey {
		get;
		set;
	}

	#endregion

	#region METHODS

	/**
	 * Returns localized string for given keys
	 */
	public string GetLocalizedString(params string[] keys) {
		if (keys.Length == 1) {
			return _GetLocalizedString (keys [0]);
		} else if (keys.Length > 0) {
			string[] translated = new string[keys.Length - 1];
			if(keys.Length > 0) {
				for (int i = 1; i < keys.Length; i++) {
					translated [i] = this._GetLocalizedString (keys [i]);
				}
			}
			return string.Format (_GetLocalizedString(keys[0]), translated);
		} else {
			LogDebug (LocaleManager.WARN_ZERO_LENGHT_KEYS);
			return string.Empty;
		}
	}

	public void SelectLocale(SystemLanguage lang) {
		PlayerPrefs.SetString (LocaleManager.PREFS_NAME, this.SystemLanguageToLocaleKey (lang));
		//PlayerPrefs.Flush ();
		this.SelectedLocalesKey = this.FallBackKey(this.SystemLanguageToLocaleKey (lang));
		this.ChoosenLanguage = this.LocaleKeyToSystemLanguage (this.SelectedLocalesKey);

		LogDebug (LocaleManager.DBG_LOCALES_SELECTED, this.SelectedLocalesKey);

	}

	protected LocaleManager(){}

	string _GetLocalizedString(string s) {
		#if LOCALE_VERBOSE
		if(!this.SelectedLocales.ContainsKey(s)) {
			Debug.LogWarningFormat(LocaleManager.WARN_KEY_NOT_FOUND, s);
		}
		#endif
		return this.SelectedLocales.ContainsKey(s) ? this.SelectedLocales [s] : s;
	}

	void Awake() {
		if (instance == null) {
			instance = this;
			Object.DontDestroyOnLoad (base.gameObject);

			this.LoadLocale ();
			string lastChoosenLocale = PlayerPrefs.GetString (LocaleManager.PREFS_NAME, string.Empty);

			// cLangEnum is locale name we will try to parse from PlayerPrefs
			SystemLanguage cLangEnum = Application.systemLanguage;
			bool fallback = string.IsNullOrEmpty (lastChoosenLocale);

			if (!fallback) {
				try {
					cLangEnum = this.LocaleKeyToSystemLanguage (lastChoosenLocale);
#pragma warning disable 0168  // Variable is declared but never used
                } catch ( System.Exception e) {
#pragma warning restore 0168  // Variable is declared but never used
                    fallback = true;
				}
			}

			if (fallback) {
				this.SelectLocale (Application.systemLanguage);
				if(!this.AllLoadedLocales.ContainsKey(SystemLanguageToLocaleKey(Application.systemLanguage))) {
					this.SelectLocale (this.defaultFallbackLanguage);
				}
				//this.SelectLocale (this.defaultFallbackLanguage);
			} else {
				this.SelectLocale (cLangEnum);
			}

		} else if (instance != this) {
			Object.Destroy (base.gameObject);
		}
	}


	// TODO: Handle parsing error better ...
	void LoadLocale() {
		if (this.csvFile == null) {
			Debug.LogError (LocaleManager.ERR_NO_FILE_ATTACHED);
			Debug.Break ();
		} else {
			this.AllLoadedLocales = new Dictionary<string, Dictionary<string, string>> ();
			string allLocaleTexts = csvFile.text;

            string firstRow = "";;
            string[] columns = null;
            string[] rows = null;

            try {
                // Parsing locale
                firstRow = allLocaleTexts.Substring(0, allLocaleTexts.IndexOf(LocaleManager.LINE_SEPARATOR_WINDOWS));
                columns = firstRow.Split (LocaleManager.CSV_SEPARATOR);
                rows = allLocaleTexts.Split (new string [] { LocaleManager.LINE_SEPARATOR_WINDOWS }, System.StringSplitOptions.None);
            }
            catch {
                firstRow = allLocaleTexts.Substring(0, allLocaleTexts.IndexOf(LocaleManager.LINE_SEPARATOR_UNIX));
                columns = firstRow.Split (LocaleManager.CSV_SEPARATOR);
                rows = allLocaleTexts.Split (new string [] { LocaleManager.LINE_SEPARATOR_UNIX }, System.StringSplitOptions.None);
            }
			

            if(rows.Length == 0)
                Debug.Log("Kapa jasia");

			this.LogDebug (LocaleManager.DBG_FOUND_KEYS, (rows.Length - 1).ToString ());

			/**
			 * Split rows
			 */
			List<string[]> splittedRows = new List<string[]> ();
			for (int i = 1; i < rows.Length; i++) {
				splittedRows.Add (rows [i].Split(LocaleManager.CSV_SEPARATOR));
			}

			/**
			 * First column is KEY
			 * Second column is ignored (it's the description info for tranlation)
			 * Start with third column
			 */
			for (int i = 2; i < columns.Length; i++) {
				if (!string.IsNullOrEmpty (columns [i])) {
					this.LogDebug (LocaleManager.DBG_FOUND_LANGS, rows[0]);

					// Create Lang structure
					Dictionary<string, string> currLocale = new Dictionary<string, string> ();
					this.AllLoadedLocales.Add (PrepareSingleCell(columns [i]), currLocale);

					// Fill with datas
					for (int j = 0; j < splittedRows.Count; j++) {
						string[] currRow = splittedRows [j];
						string preparedKey = PrepareSingleCell (currRow [0]);
						if (currLocale.ContainsKey (preparedKey)) {
							Debug.LogWarningFormat ("Locales file corrupted. \"{0}\" already exists in dictionary", preparedKey);
						} else {
							if (currRow.Length > 1) {
								currLocale.Add (
									preparedKey,
									PrepareSingleCell (currRow [i])
								);
							}
						}
					}
				}
			}
		}
	}

	string PrepareSingleCell(string s) {
		if (s.StartsWith ("\"") && s.EndsWith ("\"")) {
			s = s.Substring (1, s.Length - 2);
		}

		s = s.Replace ("\\\"", "\"");
		s = s.Replace ("\\n", "\n");

		return s;
	}

	string SystemLanguageToLocaleKey(SystemLanguage choosenLang) {
		switch(choosenLang) {
			case SystemLanguage.Polish: return "Polish";
		}
		return System.Enum.GetName (typeof(SystemLanguage), choosenLang);
	}

	SystemLanguage LocaleKeyToSystemLanguage(string lang) {
		
		return (SystemLanguage)System.Enum.Parse (typeof(SystemLanguage), lang);
	}

	/**
	 * return the same key if it exists in loaded locales,
	 * otherwise returns default locale key
	 */
	string FallBackKey(string proposedLocaleKey) {
		if (this.AllLoadedLocales != null && this.AllLoadedLocales.ContainsKey (proposedLocaleKey)) {
			return proposedLocaleKey;
		} else {
			return SystemLanguageToLocaleKey(this.defaultFallbackLanguage);
		}
	}

	void LogDebug(string message, params string[] messages) {
		#if LOCALE_VERBOSE
		Debug.LogFormat(string.Concat("<b><color='green'>", base.GetType().Name, ": </color></b>", message), messages);
		#endif
	}

	#endregion
}
