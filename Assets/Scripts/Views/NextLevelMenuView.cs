﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using UnityEngine.UI;

public class NextLevelMenuView : WindowUIView{

	public const string NEXT_LEVEL_BUTTON_CLICKED = "NextLevelMenuView.NextLevelButton.Clicked";
	public const string	BACK_TO_MAIN_MENU_BUTTON_CLICKED = "NextLevelMenuView.BackToMainMenuButton.Clicked";


	[SerializeField] Button nextLevelButton;
	[SerializeField] Button backToMainMenuButton;

	public void Init(){
		SetNextLevelButton ();
		SetBackToMainMenuButton ();
	}

	public void SetNextLevelButton(){
		nextLevelButton.onClick.RemoveAllListeners();
		nextLevelButton.onClick.AddListener (() => {
			Debug.Log("NextLevelMenu Next Clicked");
			Notify(NEXT_LEVEL_BUTTON_CLICKED);
		});
	}

	public void SetBackToMainMenuButton(){
		backToMainMenuButton.onClick.RemoveAllListeners();
		backToMainMenuButton.onClick.AddListener (() => {
			Debug.Log("NextLevelMenu Back Clicked");
			Notify(BACK_TO_MAIN_MENU_BUTTON_CLICKED);
		});
	}

}
