﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using System;

public class GameView : View<BreakoutApplication> {

	[Header("Scene References")]
	[SerializeField] MainMenuView mainMenu;
	public MainMenuView MainMenuView { get { return mainMenu; } }

	[SerializeField] SettingsGameView settingsGameView;
	public SettingsGameView SettingsGameView { get { return settingsGameView; } }

	[SerializeField] NextLevelMenuView nextLevelMenuView;
	public NextLevelMenuView NextLevelMenuView { get { return nextLevelMenuView; } }

	[SerializeField] EndGameView endGameView;
	public EndGameView EndGameView { get { return endGameView; } }

	[SerializeField] WindowUIView windowUIView;
	public WindowUIView WindowUIView { get { return windowUIView; } }

	[SerializeField] BackgroundView backgroundView;
	public BackgroundView BackgroundView{ get { return backgroundView; } }

	[SerializeField] PlayerView playerView;
	public PlayerView PlayerView{ get { return playerView; } }

	[SerializeField] PlayerPointsView playerPointsView;
	public PlayerPointsView PlayerPointsView { get { return playerPointsView; } }

	[SerializeField] PlayerLivesView playerLivesView;
	public PlayerLivesView PlayerLivesView { get { return playerLivesView; } }

	[SerializeField] Transform holderOfBrickViews;
	public Transform HolderOfBrickViews { get { return holderOfBrickViews; } }


	[Header("Prefab References")]
	[SerializeField] GameObject brickViewPrefab;
	[SerializeField] GameObject bulletViewPrefab;
	[SerializeField] GameObject explosionParticlePrefab;


	Dictionary<Brick,BrickView> brickViews = new Dictionary<Brick,BrickView>();
	public Dictionary<Brick, BrickView> BrickViews { get { return brickViews; } }

	public void SetupGameBoard(){
		Debug.Log ("SetupGameBoard Liczba brickViews " + brickViews.Count);
		// instantiate
		foreach (Brick brick in app.model.GameModel.Bricks) {
			if (brick.LevelIndex == app.model.GameModel.GetLevelIndex()) {
				GameObject go = Instantiate (brickViewPrefab, brick.Position, Quaternion.identity, holderOfBrickViews) as GameObject;
				BrickView brickView = go.GetComponent<BrickView> ();
				brickView.Init (brick);
				brickViews.Add (brick, brickView);
			}
		}

	}

	public void InstantiateBullet(PlayerView playerView){
		GameObject go = Instantiate (bulletViewPrefab, playerView.transform.position, Quaternion.identity) as GameObject;
		BulletView bulletView = go.GetComponent<BulletView> ();
		bulletView.Init ();
	}

	public void Boom(Vector3 position){
		Debug.Log ("Boom");
		Instantiate (explosionParticlePrefab, position, Quaternion.identity);
	}

	public void UpdateBricks(){
		List<BrickView> bv = new List<BrickView> ();

		foreach (KeyValuePair<Brick, BrickView> brickModelAndView in brickViews) {
			bv.Add (brickModelAndView.Value);
		}

		foreach (var item in bv) {
			item.UpdateView ();
		}
			
	



//		if (brickViews != null && brickViews.Count > 0) {
//			foreach (KeyValuePair<Brick, BrickView> brickModelAndView in brickViews) {
//				brickModelAndView.Value.UpdateView ();
//			}
//		}
	}
		
	public void DestroyBottomBricksView(List<Brick> bottomBricks){
		Debug.Log ("DestroyBrickView");
		foreach (Brick brick in bottomBricks) {
			BrickView brickView = BrickViews [brick];
			if(brickViews.ContainsKey(brick)){
			brickViews.Remove (brickView.MyBrick);
			Destroy (brickView.gameObject);
			}
		}
	}

	public void DestroyBrickView(Brick brick){
		Debug.Log ("DestroyBrickView");
		BrickView brickView = BrickViews [brick];
		if (brickViews.ContainsValue(brickView)) {
			brickViews.Remove(brickView.MyBrick);
			Destroy (brickView.gameObject);
		} 
	}

	public void ResetView(){
		Debug.Log ("ResetView");

		foreach (Transform child in holderOfBrickViews) {
			GameObject.Destroy(child.gameObject);
		}


		brickViews.Clear ();


	}
		
}