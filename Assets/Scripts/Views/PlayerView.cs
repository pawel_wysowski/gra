﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class PlayerView : WindowUIView {




	public const string BULLET_SHOT = "PlayerView.BulletShot";

	[SerializeField] SpriteRenderer mySpriteRenderer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		SetMySpriteRenderer ();



		float translation = Input.GetAxis("Horizontal") * app.model.GameModel.PlayerMovementSpeed;
		translation *= Time.deltaTime;
		transform.Translate(translation, 0, 0);

		Vector3 clampedPosition = transform.position;
		clampedPosition.x = Mathf.Clamp(transform.position.x, -6.0f, 6.0f);
		transform.position = clampedPosition;


		if (Input.GetKeyDown (KeyCode.Space)) {
			Debug.Log ("Spacja nacisniete");
			Notify (BULLET_SHOT);
		}



	}
		

	void SetMySpriteRenderer(){
		mySpriteRenderer.sprite = app.view.GetPlayerSprite (app.model.GameModel.PlayerLives);
	}



}
