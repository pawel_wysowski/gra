﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class BrickView : View<BreakoutApplication>
{

	public const string BULLET_HIT = "BrickView.BulletHitBrick";
	public const string PLAYER_HIT = "BrickView.PlayerHit";

	Brick myBrick;
	public Brick MyBrick { get { return myBrick; } }

	BrickConfig myBrickConfig;
	public BrickConfig MyBrickConfig{ get { return myBrickConfig; } }


	[SerializeField] SpriteRenderer myRenderer;

	public void Init (Brick brick)
	{
		this.myBrick = brick;
		this.myBrickConfig = app.model.GameModel.BrickConfigModel.GetBrickConfig (brick.ConfigIndex);

		SetLevelIndex ();
		SetMyRenderer ();
	}
		

	public void UpdateView(){
		this.transform.position = myBrick.Position;
	}

	void OnTriggerEnter2D(Collider2D col){

		if (col.gameObject.tag == "Bullet") {
			Notify (BULLET_HIT, col.gameObject.GetComponent<BulletView>());
		}
	
	}

	void SetLevelIndex(){
		myBrick.LevelIndex = MyBrickConfig.Level;
	}

	void SetMyRenderer(){
		myRenderer.sprite = MyBrickConfig.Image;
	}
}
