﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using UnityEngine.UI;

public class MainMenuView : WindowUIView {

	public const string START_BUTTON_CLICKED = "MainMenuView.StartGameButton.Clicked";
	public const string SETTINGS_BUTTON_CLICKED = "MainMenuView.SettingsButton.Clicked";
	public const string EXIT_BUTTON_CLICKED = "MainMenuView.ExitGameButton.Clicked";

	[SerializeField] Button startGameButton;
	[SerializeField] Button exitGameButton;
	[SerializeField] Button settingsGameButton;


	public void Init(){
		SetStartGameButton ();
		SetExitGameButton ();
		SetSettingsGameButton ();
	}

	void SetStartGameButton(){
		startGameButton.onClick.RemoveAllListeners();
		startGameButton.onClick.AddListener (() => {
			Debug.Log("MainMenuView Start Clicked");
			Notify(START_BUTTON_CLICKED);
		});
	}

	void SetSettingsGameButton(){
		settingsGameButton.onClick.RemoveAllListeners ();
		settingsGameButton.onClick.AddListener (() => {
			Debug.Log ("MainMenuView Settings Clicked");
			Notify (SETTINGS_BUTTON_CLICKED);
		});
	}

	void SetExitGameButton(){
		exitGameButton.onClick.RemoveAllListeners();
		exitGameButton.onClick.AddListener (() => {
			Debug.Log("MainMenuView Exit Clicked");
			Notify(EXIT_BUTTON_CLICKED);
		});
	}

}
