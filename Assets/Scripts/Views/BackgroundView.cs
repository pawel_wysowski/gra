﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class BackgroundView : WindowUIView {

	[SerializeField] SpriteRenderer myBackgroundSpriteRenderer;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		SetMySpriteRenderer ();
	}


	void SetMySpriteRenderer(){
		myBackgroundSpriteRenderer.sprite = app.view.GetBackgroundSprite (app.model.GameModel.level);
	}

}
