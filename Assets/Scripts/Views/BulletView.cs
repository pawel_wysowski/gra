﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class BulletView : View<BreakoutApplication>  {

	[SerializeField] Rigidbody2D myRigidbody;


	public void Init(){

		myRigidbody.AddForce (new Vector2 (0, app.model.GameModel.BulletMovementSpeed));

	
	}
}
