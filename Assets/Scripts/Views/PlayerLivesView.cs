﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using UnityEngine.UI;

public class PlayerLivesView : WindowUIView{

	[SerializeField] Text playerLivesText;

	public void Init(){
		UpdateLivesView ();
		this.gameObject.SetActive (true);
	}
	public void UpdateLivesView(){
		playerLivesText.text = string.Format (LocaleManager.Instance.GetLocalizedString("PLAYER_LIVES"), app.model.GameModel.PlayerLives);
	}
}
