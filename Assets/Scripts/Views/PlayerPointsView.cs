﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using UnityEngine.UI;

public class PlayerPointsView : WindowUIView {

	[SerializeField] Text playerPointsText;

	public void Init(){
		UpdateView ();
		this.gameObject.SetActive (true);
	}

	public void UpdateView(){
		playerPointsText.text = string.Format (LocaleManager.Instance.GetLocalizedString("PLAYER_POINTS"), app.model.GameModel.PlayerPoints);

	}

}
