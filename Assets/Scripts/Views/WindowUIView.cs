﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class WindowUIView : View<BreakoutApplication> {

	public virtual void Show(){
		this.gameObject.SetActive (true);
	}

	public virtual void Hide(){
		this.gameObject.SetActive (false);
	}


}
