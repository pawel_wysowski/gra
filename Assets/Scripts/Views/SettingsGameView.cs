﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using UnityEngine.UI;

public class SettingsGameView : WindowUIView {

	public const string VOLUME_UP_BUTTON_CLICKED = "SettingsGameView.VolumeUpButton.Clicked";
	public const string VOLUME_DOWN_BUTTON_CLICKED = "SettingsGameView.VolumeDownButton.Clicked";
	public const string CHANGE_LANGUAGE_BUTTON_CLICKED = "SettingsGameView.LanguageButton.Clicked";
	public const string BACK_TO_MAIN_MENU_BUTTON_CLICKED = "SettingsGameView.BackToMainMenu.Clicked";
	public const string MUSIC_VOLUME_UP_BUTTON_CLICKED = "SettingsGameView.MusicVolumeUp.Clicked";
	public const string MUSIC_VOLUME_DOWN_BUTTON_CLICKED = "SettingsGameView.MusicVolumeDown.Clicked";

	[SerializeField] Button volumeUpButton;
	[SerializeField] Button volumeDownButton;
	[SerializeField] Button changeLanguageButton;
	[SerializeField] Button backToMainMenuButton;
	[SerializeField] Button musicVolumeUpButton;
	[SerializeField] Button musicVolumeDownButton;


	void Awake(){
	}

	public void Init(){
		SetVolumeUpButton ();
		SetVolumeDownButton ();
		SetMusicVolumeUpButton ();
		SetMusicVolumeDownButton ();
		SetChangeLanguageButton ();
		SetBackToMainMenuButton ();
	}

	void SetVolumeUpButton(){
		volumeUpButton.onClick.RemoveAllListeners ();
		volumeUpButton.onClick.AddListener (() => {
			Debug.Log ("SettingsGameView VolumeUp Clicked");
			Notify (VOLUME_UP_BUTTON_CLICKED);
		});
	}

	void SetVolumeDownButton(){
		volumeDownButton.onClick.RemoveAllListeners ();
		volumeDownButton.onClick.AddListener (() => {
			Debug.Log ("SettingsGameView VolumeDown Clicked");
			Notify (VOLUME_DOWN_BUTTON_CLICKED);
		});
	}

	void SetChangeLanguageButton(){
		changeLanguageButton.onClick.RemoveAllListeners ();
		changeLanguageButton.onClick.AddListener (() => {
			Debug.Log ("SettingsGameView ChangeLanguage Clicked");
			Notify (CHANGE_LANGUAGE_BUTTON_CLICKED);
		});
	}

	void SetBackToMainMenuButton(){
		backToMainMenuButton.onClick.RemoveAllListeners ();
		backToMainMenuButton.onClick.AddListener (() => {
			Debug.Log ("SettingsGameView BackToMainMenu Clicked");
			Notify (BACK_TO_MAIN_MENU_BUTTON_CLICKED);
		});
	}

	void SetMusicVolumeUpButton (){
		musicVolumeUpButton.onClick.RemoveAllListeners ();
		musicVolumeUpButton.onClick.AddListener (() => {
			Debug.Log ("SettingsGameView MusicVolumeUp Clicked");
			Notify (MUSIC_VOLUME_UP_BUTTON_CLICKED);
		});
	}

	void SetMusicVolumeDownButton (){
		musicVolumeDownButton.onClick.RemoveAllListeners ();
		musicVolumeDownButton.onClick.AddListener (() => {
			Debug.Log ("SettingsGameView MusicVolumeDown Clicked");
			Notify (MUSIC_VOLUME_DOWN_BUTTON_CLICKED);
		});
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
