﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using UnityEngine.UI;

public class EndGameView : WindowUIView{

	public const string	BACK_TO_MAIN_MENU_BUTTON_CLICKED = "EndGameView.BackToMainMenuButton.Clicked";

	[SerializeField] Button backToMainMenuButton;
	[SerializeField] Text highScoreText;
	[SerializeField] Text yourScoreText;

	public void Init(){
		SetBackToMainMenuButton ();
	}

	public void SetBackToMainMenuButton(){
		backToMainMenuButton.onClick.RemoveAllListeners();
		backToMainMenuButton.onClick.AddListener (() => {
			Debug.Log("EndGameView Back Clicked");
			Notify(BACK_TO_MAIN_MENU_BUTTON_CLICKED);
		});
	}

	public void UpdateHighScore(){
		highScoreText.text = string.Format (LocaleManager.Instance.GetLocalizedString("HIGHSCORE_TEXT"), app.model.GameModel.HighScore);
		Debug.Log ("HIGHSCORE_UPDATE");
	}

	public void UpdateYourScore(){
		yourScoreText.text = string.Format (LocaleManager.Instance.GetLocalizedString("YOURSCORE_TEXT"), app.model.GameModel.PlayerPoints);
		Debug.Log ("YOURSCORE_UPDATE");
	}

}