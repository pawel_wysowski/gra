﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class GameController : Controller<BreakoutApplication> {

	void Start(){
		Init ();
	}

	public override void OnNotification (string p_event, Object p_target, params object[] p_data)
	{
		switch (p_event) {
		case MainMenuView.START_BUTTON_CLICKED:
			MainMenuView_StartButtonClicked ();
			break;
		case MainMenuView.EXIT_BUTTON_CLICKED:
			MainMenuView_ExitButtonClicked ();
			break;
		case MainMenuView.SETTINGS_BUTTON_CLICKED:
			MainMenuView_SettingsButtonClicked ();
			break;
		case SettingsGameView.BACK_TO_MAIN_MENU_BUTTON_CLICKED:
			SettingsGameView_BackToMainMenuButtonClicked ();
			break;
		case SettingsGameView.VOLUME_UP_BUTTON_CLICKED:
			SettingsGameView_VolumeUpButtonClicked ();
			break;
		case SettingsGameView.VOLUME_DOWN_BUTTON_CLICKED:
			SettingsGameView_VolumeDownButtonClicked ();
			break;
		case SettingsGameView.CHANGE_LANGUAGE_BUTTON_CLICKED:
			SettingsGameView_ChangeLanguageButtonClicked ();
			break;
		case SettingsGameView.MUSIC_VOLUME_UP_BUTTON_CLICKED:
			SettingsGameView_MusicVolumeUpButtonClicked ();
			break;
		case SettingsGameView.MUSIC_VOLUME_DOWN_BUTTON_CLICKED:
			SettingsGameView_MusicVolumeDownButtonClicked ();
			break;
		case NextLevelMenuView.NEXT_LEVEL_BUTTON_CLICKED:
			NextLevelMenuView_NextLevelButtonClicked ();
			break;
		case NextLevelMenuView.BACK_TO_MAIN_MENU_BUTTON_CLICKED:
			NextLevelMenuView_BackToMainMenuButtonClicked ();
			break;
		case EndGameView.BACK_TO_MAIN_MENU_BUTTON_CLICKED:
			EndGameView_BackToMainMenuButtonClicked ();
			break;
		case PlayerView.BULLET_SHOT:
			PlayerView_BulletShot ((PlayerView)p_target);
			break;
		case BrickView.BULLET_HIT:
			BrickView_BulletHit ((BrickView)p_target, (BulletView)p_data [0]);
			break;
		case GameModel.BRICK_STRENTGH_CHANGED:
			GameModel_BrickStrengthChanged ((Brick)p_data [0]);
			break;
		case GameModel.PLAYER_POINTS_CHANGED:
			GameModel_PlayerPointsChanged ();
			break;	
		case GameModel.TIME_LEFT_ZERO:
			GameModel_TimeLeftZero ();
			break;
		case GameModel.BRICKS_CHANGED:
			GameModel_BricksChanged ();
			break;
		case GameModel.BRICK_ON_BOTTOM:
			GameModel_BrickOnBottom ((List<Brick>)p_data[0]);
			break;
		case GameModel.PLAYER_LIVES_CHANGED:
			GameModel_PlayerLivesChanged ();
			break;
		case GameModel.NEXT_LEVEL:
			GameModel_NextLevel ();
			break;
		case GameModel.STRENGTH_ADDED_TO_BRICK:
			GameModel_StrengthAddedToBrick((Brick)p_data[0]);
			break;
		case GameModel.BRICK_DESTROYED:
			GameModel_BrickDestroyed ((Brick)p_data [0]);
			break;
		case GameModel.BRICKS_DESTROYED:
			GameModel_BricksDestroyed ();
			break;
		case GameModel.END_GAME:
			GameModel_EndGame ();
			break;
		}

	}

	void Init(){
		app.view.GameView.MainMenuView.Show();
		app.view.GameView.MainMenuView.Init ();
		SoundManager.PlayMusic (EMusicTrack.eTrackType.MainTrack);
	}


	void MainMenuView_StartButtonClicked(){
		Debug.Log ("START_BUTTON_CLICKED");
		app.view.GameView.BackgroundView.Show();
		app.model.GameModel.ChangeState ();
		Debug.Log (app.model.GameModel.CurrentState);
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		app.model.GameModel.ResetLevel ();
		app.model.GameModel.AddBricksToList ();
		app.view.GameView.MainMenuView.Hide();
		app.view.GameView.PlayerView.Show();
		Debug.Log(app.model.GameModel.GetLevelIndex ());
		app.view.GameView.SetupGameBoard ();
		app.view.GameView.PlayerPointsView.Init ();
		app.view.GameView.PlayerLivesView.Init ();

	}

	void MainMenuView_SettingsButtonClicked(){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		app.view.GameView.MainMenuView.Hide();
		app.view.GameView.SettingsGameView.Show();
		app.view.GameView.SettingsGameView.Init ();

	}

	void SettingsGameView_BackToMainMenuButtonClicked (){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		app.view.GameView.SettingsGameView.Hide();
		app.view.GameView.MainMenuView.Show();
	}

	void MainMenuView_ExitButtonClicked (){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		Debug.Log ("EXIT_BUTTON_CLICKED");
	}

	void SettingsGameView_MusicVolumeUpButtonClicked (){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		Debug.Log (SoundManager.Instance ().GetMusicVolume ());
		SoundManager.Instance ().MusicVolumeUp ();
	}

	void SettingsGameView_MusicVolumeDownButtonClicked (){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		Debug.Log (SoundManager.Instance ().GetMusicVolume ());
		SoundManager.Instance ().MusicVolumeDown ();
	}

	void SettingsGameView_VolumeUpButtonClicked (){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		Debug.Log(SoundManager.Instance ().GetSoundVolume ());
		SoundManager.Instance ().VolumeUp ();
	}

	void SettingsGameView_VolumeDownButtonClicked (){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		Debug.Log(SoundManager.Instance ().GetSoundVolume ());
		SoundManager.Instance ().VolumeDown ();
	}

	void SettingsGameView_ChangeLanguageButtonClicked (){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		app.model.GameModel.ChangeLanguage ();
	}

	void NextLevelMenuView_NextLevelButtonClicked(){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		app.view.GameView.BackgroundView.Show();
		app.model.GameModel.AddBricksToList ();
		app.view.GameView.PlayerView.Show();
		app.view.GameView.PlayerPointsView.Show();
		app.view.GameView.PlayerLivesView.Show();
		app.view.GameView.NextLevelMenuView.Hide();
		app.view.GameView.SetupGameBoard ();
		app.model.GameModel.ChangeState ();
		Debug.Log (app.model.GameModel.CurrentState);

	}

	void NextLevelMenuView_BackToMainMenuButtonClicked(){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		app.view.GameView.NextLevelMenuView.Hide();
		app.view.GameView.MainMenuView.Show();
		app.view.GameView.BackgroundView.Hide();
	}

	void PlayerView_BulletShot(PlayerView playerView){
		Debug.Log ("BULLET_SHOT");
		//app.view.GameView.InstantiateBullet (app.view.GameView.PlayerView);
		//lub
		SoundManager.Play(Sound.eSoundType.BulletFired);
		app.view.GameView.InstantiateBullet (playerView);
	}

	void BrickView_BulletHit (BrickView brickView, BulletView bulletView){
		Debug.Log ("BULLET_HIT");

		// albo tak
		//brick.MyBrick.SubstractStrength ();
		//brick.UpdateView ();

		//albo tak
		app.model.GameModel.SubstractStrength (brickView.MyBrick);
		app.view.GameView.Boom (brickView.transform.position);

		Destroy (bulletView.gameObject);
	}
		

	void GameModel_BrickStrengthChanged (Brick brick){
		app.view.GameView.BrickViews [brick].UpdateView ();
	}

	void GameModel_BrickDestroyed (Brick brick){
		SoundManager.Play (Sound.eSoundType.BrickDestroy);
		app.model.GameModel.AddPlayerPoint (app.model.GameModel.BrickConfigModel.GetBrickConfig(brick.ConfigIndex).Points); // na podstawie konfigu, bo rozne bricki dodaja rozne pointy
		app.view.GameView.DestroyBrickView (brick);
		app.model.GameModel.DestroyBrickModel (brick);

	}

	void GameModel_BricksChanged (){
		app.view.GameView.UpdateBricks ();
	}

	void GameModel_TimeLeftZero (){
		Debug.Log ("TIME_LEFT_ZERO");
		app.model.GameModel.MoveDownBricks ();
	}

	void GameModel_PlayerPointsChanged(){
		app.view.GameView.PlayerPointsView.UpdateView ();
	}

	void GameModel_BrickOnBottom (List<Brick> bottomBricks){
		Debug.Log ("Bottom bricks count: " + bottomBricks.Count);
		app.model.GameModel.PlayerLivesDown (bottomBricks);
		app.model.GameModel.DestroyBottomBricks (bottomBricks);
		app.view.GameView.DestroyBottomBricksView (bottomBricks);
	}

	void GameModel_PlayerLivesChanged (){
		Debug.Log ("PLAYER_LIFE_DOWN");
		app.view.GameView.PlayerLivesView.UpdateLivesView ();
	}
	void GameModel_GameOver(){
		Debug.Log ("GAME_OVER");
		app.model.GameModel.ChangeState ();
		app.view.GameView.ResetView ();
		app.view.GameView.NextLevelMenuView.Hide();
		app.view.GameView.MainMenuView.Show ();
		Debug.Log (app.model.GameModel.CurrentState);
		app.model.GameModel.ResetPlayerPoints ();
		app.model.GameModel.ResetPlayerLives ();
		app.model.GameModel.ResetLevel ();
		app.view.GameView.PlayerView.Hide();
		app.view.GameView.PlayerPointsView.Hide();
		app.view.GameView.PlayerLivesView.Hide();
	}

	void GameModel_BricksDestroyed(){
		app.model.GameModel.CheckLevelValue ();
	}

	void GameModel_NextLevel (){
		Debug.Log ("NEXT_LEVEL");
		app.view.GameView.BackgroundView.Hide();
		app.model.GameModel.IncreaseLevel ();
		app.model.GameModel.ChangeState ();
		app.view.GameView.NextLevelMenuView.Init ();
		app.view.GameView.NextLevelMenuView.Show ();
		app.model.GameModel.ResetPlayerLives ();
		app.view.GameView.PlayerView.Hide();
		app.view.GameView.PlayerPointsView.Hide();
		app.view.GameView.PlayerLivesView.Hide();

	}
		
	void GameModel_EndGame(){
		app.view.GameView.BackgroundView.Hide ();;
		app.view.GameView.EndGameView.UpdateYourScore ();
		app.model.GameModel.ChangeState ();
		app.view.GameView.EndGameView.Init ();
		app.view.GameView.EndGameView.Show ();
		app.view.GameView.ResetView ();
		app.model.GameModel.ResetPlayerLives ();
		app.view.GameView.EndGameView.Show();
		app.view.GameView.PlayerView.Hide();
		app.model.GameModel.SetHighScore ();
		app.view.GameView.EndGameView.UpdateHighScore ();
		app.model.GameModel.ResetPlayerPoints ();
		app.view.GameView.PlayerPointsView.Hide();
		app.view.GameView.PlayerLivesView.Hide();
	}

	void EndGameView_BackToMainMenuButtonClicked(){
		SoundManager.Play (Sound.eSoundType.ButtonClick);
		app.view.GameView.EndGameView.Hide();
		app.view.GameView.MainMenuView.Show();
	}

	void GameModel_StrengthAddedToBrick(Brick brickModel){
		Debug.Log ("StrengthAddedToBrick");
		app.view.GameView.UpdateBricks ();

	}

}

