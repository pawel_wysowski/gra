﻿using UnityEngine;


// Jak używać?
// SoundManager.Play(Sound.eSoundType.*);

public static class Sound {
    public enum eSoundType {
        ButtonClick,
		BrickDestroy,
		BulletFired,
    }
}

public static class EMusicTrack {
    public enum eTrackType {
        MainTrack,
    }
}

public class SoundManager : MonoBehaviour {
    // wlacz/wylacz mozliwosc dzwieku w SM
    public static bool SoundOn = true;

    private static SoundManager instance;
    //= null;

    public AudioClip[] Sounds;
    public AudioClip[] Tracks;


    public AudioSource TrackAudioSource;
    public AudioSource SoundAudioSource;

    private float TrackVolume = 0.25f;
    private float SoundVolume = 0.5f;
    

    void Awake() {
        if (instance == null) {
            DontDestroyOnLoad(gameObject);
            instance = this;
            // zacznij grać muzykę
            //SoundManager.PlayMusic(EMusicTrack.eTrackType.ShelterTrack);
        }
        else if (instance != this) {
            Destroy(gameObject);
        }        

        TrackVolume = UnityEngine.PlayerPrefs.GetFloat("TrackVolume", 0.25f);
        SoundVolume = UnityEngine.PlayerPrefs.GetFloat("SoundVolume", 0.5f);
                    
        Apply();
    }


    public static SoundManager Instance() {
        if (instance == null) {
            instance = FindObjectOfType(typeof(SoundManager)) as SoundManager;
        }

        return instance;
    }

	public void VolumeUp(){
		float volume = SafeGetSoundVolume ();
		volume++;
		SafeSetSoundVolume (volume);
	}

	public void VolumeDown(){
		float volume = SafeGetSoundVolume ();
		volume--;
		SafeSetSoundVolume (volume);
	}

	public void MusicVolumeUp(){
		float volume = SafeGetMusicVolume ();
		volume++;
		SafeSetMusicVolume (volume);
	}

	public void MusicVolumeDown(){
		float volume = SafeGetMusicVolume ();
		volume--;
		SafeSetMusicVolume (volume);
	}

    public static void Play(AudioClip clip) {

        SoundManager ins = SoundManager.Instance();
        if (ins) {
            if (SoundOn == true) {
                ins.SoundAudioSource.volume = ins.GetSoundVolume();
                ins.SoundAudioSource.PlayOneShot(clip);
            }
        }
    }

    public static void PlayMusic(AudioClip clip) {
        SoundManager ins = SoundManager.Instance();
        if (ins) {
            if (SoundOn == true) {
                if (clip == ins.TrackAudioSource.clip)
                    return;

                ins.TrackAudioSource.loop = true;
                ins.TrackAudioSource.clip = clip;
                ins.TrackAudioSource.volume = ins.GetMusicVolume();
                ins.TrackAudioSource.Play();
            }
        }


    }

    public static void StopAll() {
        SoundManager ins = SoundManager.Instance();
        if (ins) {
            ins.TrackAudioSource.Stop();
        }
    }
    public static void StopAllClips() {
        SoundManager ins = SoundManager.Instance();
        if (ins) {
            ins.SoundAudioSource.Stop();
        }
    }

    public static void SetFadeVolumeWeight(float weight) {
        SoundManager ins = SoundManager.Instance();
        if (ins) {
            weight = 1.0f - Mathf.Clamp01(weight);
            ins.TrackAudioSource.volume = ins.GetMusicVolume() * weight;
            ins.SoundAudioSource.volume = ins.GetSoundVolume() * weight;
        }
    }

    public static void Play(Sound.eSoundType soundType) {
        SoundManager ins = SoundManager.Instance();
        if (ins) {
            Play(ins.Sounds[(int)soundType]);
        }
    }

    public static void PlayMusic(EMusicTrack.eTrackType track) {
        SoundManager ins = SoundManager.Instance();
        if (ins != null) {
            PlayMusic(ins.Tracks[(int)track]);
        }
    }

    public static void SafeSetMusicVolume(float volume) {
        SoundManager ins = SoundManager.Instance();
        if (ins != null) {
            ins.SetMusicVolume(volume);
        }
    }

    public static void SafeSetSoundVolume(float volume) {
        SoundManager ins = SoundManager.Instance();
        if (ins != null) {
            ins.SetSoundVolume(volume);
        }
    }

    public static float SafeGetMusicVolume() {
        SoundManager ins = SoundManager.Instance();
        if (ins != null) {
            return ins.GetMusicVolume();
        }
        return 1.0f;
    }

    public static float SafeGetSoundVolume() {
        SoundManager ins = SoundManager.Instance();
        if (ins != null) {
            return ins.GetSoundVolume();
        }
        return 1.0f;
    }

    public void Apply() {
        UnityEngine.PlayerPrefs.SetFloat("TrackVolume", TrackVolume);
        UnityEngine.PlayerPrefs.SetFloat("SoundVolume", SoundVolume);
//		PlayerPrefs.Flush ();

        SoundAudioSource.volume = SoundVolume;
        TrackAudioSource.volume = TrackVolume;
    }

    public void SetMusicVolume(float volume) {
        TrackVolume = volume;
        Apply();
    }

    public void SetSoundVolume(float volume) {
        SoundVolume = volume;
        Apply();
    }

    public float GetMusicVolume() {
        return TrackVolume;
    }

    public float GetSoundVolume() {
        return SoundVolume;
    }
}