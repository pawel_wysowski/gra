﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class BreakoutView : View<BreakoutApplication> {

	public GameView GameView { get { return gameView = Assert<GameView>(gameView); } }
	private GameView gameView;


	[SerializeField] Sprite[] playerSprites;
	[SerializeField] Sprite[] backgroundSprites;

	public Sprite GetPlayerSprite(int playerLives){
		if (playerLives < 2) {
			return 	 playerSprites [0];
		}

		return 	 playerSprites [1];
	}

	public Sprite GetBackgroundSprite (int level){
		if (level == 1)
			return backgroundSprites [0];
		if (level == 2)
			return backgroundSprites [1];
		if (level == 3)
			return backgroundSprites [2];
		else
			return backgroundSprites [3];
	
	}

}
