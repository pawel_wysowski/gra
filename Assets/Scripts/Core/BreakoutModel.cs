﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class BreakoutModel : Model<BreakoutApplication>{

	public GameModel GameModel { get { return gameModel = Assert<GameModel>(gameModel); } }
	private GameModel gameModel;

}
